$('.js-popup-video').fancybox();

$('.js-popup').fancybox({
  afterLoad: function() {
    lineNavInit();
    $('select').styler('destroy');
    $('select').styler();
  }
});

$('.popup__close').on('click', function(e) {
  e.preventDefault();
  $.fancybox.close();
});