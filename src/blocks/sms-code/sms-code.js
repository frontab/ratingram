$('.sms-code__input').on('input', function(e) {
  var code = $(this).val(),
      codeArray = code.split('');

  $('.sms-code__item').each(function(e) {
    var attrNum = $(this).attr('data-num'),
        symbol = $(this).attr('data-symbol');

    if ( codeArray[attrNum] ) {
      $(this).text(codeArray[attrNum]);
      $(this).addClass('active');
    } else {
      $(this).text(symbol);
      $(this).removeClass('active');
    }

    $('.sms-code__item').removeClass('selected');
    if ( $('.sms-code__item.active').length ) {
      $('.sms-code__item.active').next()
                                 .addClass('selected');
    } else {
      $('.sms-code__item:first-child').addClass('selected');
    }
  });
});