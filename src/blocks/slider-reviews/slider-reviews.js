$('.slider-reviews').owlCarousel({
  loop: false,
  arrows: false,
  autoWidth: true,
  margin: 25,
  dots: true
});

$('.slider-reviews--dots').owlCarousel({
    items: 1,
    nav: false,
    dots: true,
    margin:10,
    loop:false,
    responsive : {
      1200 : {
        margin: 25,
        autoWidth: true
      }
    }
});

$('.slider-reviews--alt').owlCarousel({
    items: 1,
    nav: false,
    dots: false,
    margin:10,
    loop:false,
    responsive : {
      1200 : {
        margin: 25,
        autoWidth: true
      }
    }
});
