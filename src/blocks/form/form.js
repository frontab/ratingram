$('select').styler();

$('.form__input-phone').mask("+7 (999) 999-99-99");

$('.form__date').datepicker({ dateFormat: "dd MM yyyy" });
$('.form__date-range').datepicker({
  range: 'period',
  range_multiple_max: 2,
  dateFormat: "dd MM yyyy"
});



var maxFileSize = 15 * 1024 * 1024; // (байт) Максимальный размер файла (15мб)
var queue = {};
var form = $('.comments__form');
var imagesList = $('.form-box__files');


if ( imagesList.length ) {
  var itemPreviewTemplate = imagesList.find('li').clone();
  itemPreviewTemplate.removeClass('template');
  imagesList.find('li.template').remove();
}

$('.form-more-files').on('click', function(e) {
  e.preventDefault();
});

$('.form__file-input').on('change', function () {
  var files = this.files;

   for (var i = 0; i < files.length; i++) {
       var file = files[i];

       if ( !file.type.match(/image\/(jpeg|jpg|png|gif)/) ) {
           alert( 'Фотография должна быть в формате jpg, png или gif' );
           continue;
       }

       if ( file.size > maxFileSize ) {
           alert( 'Размер фотографии не должен превышать 2 Мб' );
           continue;
       }

       preview(files[i]);
   }

   this.value = '';
});

// Создание превью
function preview(file) {
   var reader = new FileReader();
   reader.addEventListener('load', function(event) {
       var img = document.createElement('img');

       var itemPreview = itemPreviewTemplate.clone();

       itemPreview.find('.form-box__file-img-pic img').attr('src', event.target.result);
       itemPreview.data('id', file.name);
       itemPreview.find('.form-box__file-img-name').text(file.name);

       imagesList.append(itemPreview);

       queue[file.name] = file;

   });
   reader.readAsDataURL(file);
}
// Удаление фотографий
imagesList.on('click', '.form-box__action .btn', function (e) {
  e.preventDefault();
  var btnType = $(this).attr('data-type');

  if ( btnType == "del" ) {
    var item = $(this).closest('li'),
        id = item.data('id');
    delete queue[id];
    item.remove();
  }

  if ( btnType == "viewSelect" ) {
    $(this).hide();
    $(this).closest('.form-box__file')
           .find('.form-box__select')
           .addClass('active');

    boxSelectInit($(this));
  }
});


function boxSelectInit(btn) {
  $('.form-box__select-option').on('click', function(e) {
    e.preventDefault();
    var code = $(this).html();

    $(this).closest('.form-box__select')
           .removeClass('opened')
           .addClass('selected')
           .find('.form-box__select-val')
           .html(code);
  });

  var box = btn.closest('.form-box__file');

  box.find('.form-box__select-val')
  .on('click', function(e) {
    e.preventDefault();

    if ( $(this).closest('.form-box__select').hasClass("selected") ) {
      return false;
    }
    $(this).closest('.form-box__select')
           .toggleClass('opened')
           .closest('.form-box__file')
           .siblings('.form-box__file')
           .find('.form-box__select')
           .removeClass('opened');
  });
}

$(document).mouseup(function (e) {
  var container = $(".form-box__select");
  if (container.has(e.target).length === 0){
      container.removeClass('opened');
  }
});



$('.form-drop-files input').on('change', function(e) {
    $(e.target).parent().removeClass('hover');
    $(e.target).parent().addClass('filled');
});
$('.form-drop-files input').on('dragover', function(e) {
    $(e.target).parent().addClass('hover');
});
$('.form-drop-files input').on('dragleave', function(e) {
    $(e.target).parent().removeClass('hover');
});




var range = document.getElementById('ratingRange');
var pMin = parseInt($('#ratingRange').attr('data-min'));
var pMax = parseInt($('#ratingRange').attr('data-max'));

if ($('#ratingRange').length) {
  var ratingRange = noUiSlider.create(range, {

    range: {
        'min': pMin,
        'max': pMax
    },
    step: 1,
    start: [pMin, pMax],
    behaviour: 'tap-drag',
    connect: true,
    format: wNumb({
        decimals: 0
    })
  });

  var nodes = [
    document.getElementById('rating-range-lower'),
    document.getElementById('rating-range-upper')
  ];

  ratingRange.on('update', function (values, handle, unencoded, isTap, positions) {
    nodes[handle].value = values[handle];
});
}

$('#rating-range-lower').on('change', function () {
  ratingRange.set([this.value, null]);
});
$('#rating-range-upper').on('change', function () {
  ratingRange.set([null,this.value]);
});

labelDel();
function labelDel() {
  $('.form__label-del').on('click', function(e) {
    e.preventDefault();

    $(this).closest('.form__label')
           .remove();
  });
}
var rangePrice = document.getElementById('priceRange');
var pMin = parseInt($('#priceRange').attr('data-min'));
var pMax = parseInt($('#priceRange').attr('data-max'));

if ($('#priceRange').length) {
  var priceRange = noUiSlider.create(rangePrice, {

    range: {
        'min': pMin,
        'max': pMax
    },
    step: 1,
    start: [pMin, pMax],
    behaviour: 'tap-drag',
    connect: true,
    format: wNumb({
        decimals: 0
    })
  });

  var nodes = [
    document.getElementById('price-range-lower'),
    document.getElementById('price-range-upper')
  ];

  priceRange.on('update', function (values, handle, unencoded, isTap, positions) {
    nodes[handle].value = values[handle];
});
}

$('#price-range-lower').on('change', function () {
  priceRange.set([this.value, null]);
});
$('#price-range-upper').on('change', function () {
  priceRange.set([null,this.value]);
});


labelDel();
function labelDel() {
  $('.form__label-del').on('click', function(e) {
    e.preventDefault();

    $(this).closest('.form__label')
           .remove();
  });
}

$('.form-sel-val').on('click', function(e) {
  e.preventDefault();
  $(this).closest('.form-sel')
         .toggleClass('opened');
});

$('.form-sel__drop li span').on('click', function(e) {
  e.preventDefault();
  $(this).closest('.form-sel__drop')
         .find('li')
         .removeClass('selected');
  $(this).closest('li')
         .addClass('selected');

  var cont = $(this).html();

  $(this).closest('.form-sel')
         .find('.form-sel-val')
         .html(cont);

  $(this).closest('.form-sel')
         .removeClass('opened');
});

$(document).mouseup(function (e) {
  var container = $(".form-sel");
  if (container.has(e.target).length === 0){
      container.removeClass('opened');
  }
});