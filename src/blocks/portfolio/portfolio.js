function portfolioInit() {
  $(document).ready(function() {
    if ( $(window).width() > 1200 ) {
      $('.portfolio__items').owlCarousel('destroy');
      $('.portfolio__items').removeClass('owl-carousel');
      $('.portfolio__items').isotope({
        itemSelector: '.portfolio-item',
        masonry: {
          columnWidth: 10
        }
      });
    } else {
      $('.portfolio__items').isotope('destroy');
      $('.portfolio__items').addClass('owl-carousel');
      $('.portfolio__items').owlCarousel({
          margin: 20,
          loop:false,
          dots: false,
          autoWidth: true,
          nav: false
      });
    }
  });
}
portfolioInit();

$(window).resize(function() {
  portfolioInit();
});

$('.portfolio-item__video').fancybox();