$('.slider-agent').owlCarousel({
  autoWidth: true,
  margin: 20,
  loop:false,
  dots: false,
  nav: false,
  responsive : {
    1200 : {
      autoWidth: false,
      items: 6,
      slideBy: 1,
      nav: true
    }
  }
});