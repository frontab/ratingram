$('.profile-box__trigger').on('click', function(e) {
  e.preventDefault();
  $(this).siblings('.profile-box__drop')
         .toggleClass('active');
});

$(document).mouseup(function (e) {
    var container = $(".profile-box__rating");
    if (container.has(e.target).length === 0){
        container.find('.profile-box__drop')
                 .removeClass('active');
    }
});