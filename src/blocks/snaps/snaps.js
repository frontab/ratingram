$('.snaps').scrollbar();

$('.snaps-card__input').on('change', function() {
  var img = $(this).closest('.snaps-card')
                   .find('.snaps-card__img');

  var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            $(this).closest('.snaps-card')
                   .addClass('active');
            readURL(this,img);
            break;
        default:
            return false;
    }


});

function readURL(input,img) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      img.attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}