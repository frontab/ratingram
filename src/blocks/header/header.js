$('.header-item__drop-list').scrollbar();

$('.header-item__link-drop').on('click', function(e) {
  e.preventDefault();
  $(this).siblings('.header-item__drop')
         .toggleClass('active');
});

$(document).mouseup(function (e) {
    var container = $(".header-item");
    if (container.has(e.target).length === 0){
        container.find('.header-item__drop')
                 .removeClass('active');
    }
});