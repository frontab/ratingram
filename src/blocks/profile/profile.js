$('.profile-status__prof-item').on('click',function(e) {
  e.preventDefault();
  $(this).addClass('active')
         .siblings()
         .removeClass('active');
});

$('.card__list a').on('click', function(e) {
  e.preventDefault();

  $(this).closest('li')
         .addClass('active')
         .siblings()
         .removeClass('active');

  var itemNum = $(this).attr('data-box');

  $('.card__box').each(function() {
    if ( $(this).attr('data-box') == itemNum ) {
      $(this).addClass('active')
             .siblings()
             .removeClass('active');
    }
  });
});

$('.card__file').on('change', function(e) {
  var imgPrev = $(this).closest('.card__box')
                       .find('.card__img'),
      input = $(this),
      box = $(this).closest('.card__box');

  if ( $(this).closest('label').hasClass('isVideo') ) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      var imgPrev = $(this).closest('.card__box')
                           .find('.card__video');

      reader.onload = function(e) {
        var newVideo = e.target.result;
        imgPrev.attr('src', e.target.result);
        imgPrev.closest('.card__box')
               .addClass('edit');
        $('.card__list a').each(function(e) {
          if ( box.attr('data-box') == $(this).attr('data-box') ) {
            $(this).find('video')
                   .removeClass('hidden')
                   .attr('src',newVideo);
          }
        });
      };

      reader.readAsDataURL(this.files[0]);
    } else {
      imgPrev.closest('.card__box')
             .removeClass('edit');
    }
  } else {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        var newImg = e.target.result;
        imgPrev.attr('src', e.target.result);
        imgPrev.closest('.card__box')
               .addClass('edit');
        $('.card__list a').each(function(e) {
          if ( box.attr('data-box') == $(this).attr('data-box') ) {
            $(this).find('img')
                   .removeClass('hidden')
                   .attr('src',newImg);
          }
        });
      };

      reader.readAsDataURL(this.files[0]);
    } else {
      imgPrev.closest('.card__box')
             .removeClass('edit');
    }
  }
});