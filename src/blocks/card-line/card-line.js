function sliderInit() {
    if ( $(window).width() < 1200 ) {
        $('.card-line__slider--small').owlCarousel('destroy');
        $('.card-line__slider--small').removeClass('owl-carousel');

        $('.card-line__slider--full').owlCarousel('destroy');
        $('.card-line__slider--full').removeClass('owl-carousel');

    } else {
        $('.card-line__slider--small').addClass('owl-carousel');
        $('.card-line__slider--small').owlCarousel({
            items: 4,
            nav: true,
            dots: false,
            margin:20,
            loop:false
        });
        $('.card-line__slider--full').addClass('owl-carousel');
        $('.card-line__slider--full').owlCarousel({
            items: 6,
            nav: true,
            dots: false,
            margin:20,
            loop:false
        });
    }
}
sliderInit();
$(window).on('resize', function() {
    sliderInit();
});

$('.card-line__stats').scrollbar();

$('.card-line__chat-trigger').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.card-line')
           .find('.card-line__chat')
           .toggleClass('active');
    var text = $(this).text(),
        attr = $(this).attr('data-text');
    $(this).attr('data-text', text);
    $(this).text(attr);
});