function lineNavInit() {
  $('.line-nav').owlCarousel({
    margin:25,
    loop:false,
    autoWidth: true,
    nav: false,
    dots: false,
    mouseDrag: false
  });
}

lineNavInit();
