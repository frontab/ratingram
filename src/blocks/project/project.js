$('.project-parts__list').scrollbar();

$('.project-line__checkbox-main').on('change', function() {
  var isCheck = $(this).prop("checked");
  $('.project-lines .project-line__checkbox-input').prop("checked", isCheck);
});

$('.project-line__menu-btn').on('click', function(e) {
  e.preventDefault();

  $(this).closest('.project-line__menu')
         .toggleClass('active');
});

$(document).mouseup(function (e) {
    var container = $(".project-line__menu");
    if (container.has(e.target).length === 0){
        container.removeClass('active');
    }
});

$('.project-line__item-del').on('click', function(e) {
  e.preventDefault();

  $(this).closest('.project-line')
         .remove();
});

$('.project-line__item-edit').on('click', function(e) {
  e.preventDefault();
  $(this).closest('.project-line')
         .find('.project-line__file-name')
         .toggleClass('edit')
         .find('input')
         .focus();

  $(this).toggleClass('active');
});

$('.project-line__file-name-input').on('input', function(e) {
  var text = $(this).val(),
      span = $(this).siblings('span');

  span.text(text);
});