$('.slider-brands').owlCarousel({
  items: 3,
  slideBy: 3,
  nav: false,
  dots: true,
  margin:20,
  loop:false,
  responsive : {
    1200 : {
      margin:10,
      items: 5,
      slideBy: 5,
    }
  }
});