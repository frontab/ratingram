function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$('.promo__btn').on('click', function(e) {
  e.preventDefault();

  var box = $(this).siblings('.promo__num');

  copyToClipboard(box);
});