$('.filter-mobile__filter').on('click', function(e) {
  e.preventDefault();

  $('body').addClass('overflow');
  $('.filter__items').addClass('active');
  $('.inner__aside-mobile').addClass('active');
});

$('.filter__items-close, .inner__aside-mobile-close').on('click', function(e) {
  e.preventDefault();

  $('body').removeClass('overflow');
  $('.filter__items').removeClass('active');
  $('.inner__aside-mobile').removeClass('active');
});

$('.form__label-template select').styler('destroy');



$('.filter__add').on('click', function(e) {
  e.preventDefault();

  var newSelect = '\
  <label class="form__label form__label-template">\
  <span class="form__label-name">\
    <span class="form__label-del"></span>\
  </span>\
    <select class="form__select" data-placeholder="Выберите фильтр">\
      <option value="" label="empty"></option>\
      <option value="Название 1">Название 1</option>\
      <option value="Название 2">Название 2</option>\
      <option value="Название 3">Название 3</option>\
    </select>\
  </label>';
  $(this).closest('.form__label')
         .before(newSelect);

  $('select').styler();
  labelDel();
  labelTemplateSelect();

  $('.filter__add').closest('.form__label')
                   .hide();

  $('.form__label-template .form__label-del').on('click', function() {
    $('.filter__add').closest('.form__label')
                     .show();
  });
});

labelTemplateSelect();
function labelTemplateSelect() {
  $('.form__label-template select').on('change', function(e) {
    var val = $(this).val(),
        btnDel = '<span class="form__label-del"></span>';
    
    $(this).closest('.form__label')
           .find('.form__label-name')
           .html('')
           .append(val)
           .append(btnDel);

    labelDel();

    $(this).unbind();

    $(this).closest('.form__label-template')
           .removeClass('form__label-template');

    changeOption($(this));

    $('.filter__add').closest('.form__label')
                     .show();
  });
}

function changeOption(select) {
  var newArray = ['Вариант 1','Вариант 2','Вариант 3','Вариант 4','Вариант 5'],
      newList = '';

  for ( var i = 0; i <= newArray.length - 1; i ++ ) {
    newList = newList + '<option value="">' + newArray[i] + '</option>';
  }

  select.html('');
  select.append(newList);

  $(select).styler('destroy');
  $(select).styler();
}