$('.slider-alt').owlCarousel({
  autoWidth: true,
  slideBy: 1,
  margin: 20,
  loop:false,
  dots: false,
  nav: false,
  responsive : {
    1200 : {
      autoWidth: false,
      items: 8,
      nav: true
    }
  }
});