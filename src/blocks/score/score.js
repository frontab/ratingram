$('.score-summa__input').focus(function(e) {
  $(this).addClass('focused');
});
$('.score-summa__input').focusout(function(e) {
  $(this).removeClass('focused');
});
$('.score-summa__input').change(function(e) {
  if ( parseInt($(this).val()) ) {
    var format = String(parseInt($(this).val())).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
    $(this).val(parseInt($(this).val()));
    $(this).siblings('.score-summa__num')
           .text(format);
  } else {
    $(this).val('');
    $(this).siblings('.score-summa__num')
           .text(0);
  }
});