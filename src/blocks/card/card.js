$('.cards-slider__list').owlCarousel({
    margin: 20,
    loop:true,
    dots: false,
    autoWidth: true,
    nav: false,
    responsive : {
      1200 : {
        items: 3,
        nav: true,
        autoWidth: false
      }
  }
});

$('.card__item--video').fancybox();

$('.card__item').on('click', function(e) {
  e.preventDefault();
  if($(this).hasClass('card__item--video')) {
    return false;
  }

  var img = $(this).closest('.card__content')
                   .find('.card__img'),
      imgAt = img.attr('src'),
      imgSrc = $(this).find('img')
                      .attr('src');

  if ( $(this).index() == 1 ) {
    img.attr('src',imgSrc);
    $(this).find('img')
           .attr('src',imgAt);
  }

  if ( $(this).index() == 0 ) {
    if ( $(this).closest('.card__content').find('.card__item:nth-child(2)').hasClass('card__item--video') ) {
      img.attr('src',imgSrc);
      $(this).find('img')
             .attr('src',imgAt);
      return false;
    }

    var imgS = $(this).closest('.card__content')
                      .find('.card__item:nth-child(2)')
                      .find('img'),
        imgSSrc = imgS.attr('src');

    img.attr('src',imgSrc);
    imgS.attr('src',imgAt);
    $(this).find('img')
           .attr('src',imgSSrc);
  }
});

$('.card__favorite').on('click', function(e) {
  e.preventDefault();
  if ( $(this).hasClass('active') ) {
    $(this).removeClass('active')
           .find('.icon')
           .removeClass('icon-favorite-full')
           .addClass('icon-favorite');
  } else {
    $(this).addClass('active')
           .find('.icon')
           .removeClass('icon-favorite')
           .addClass('icon-favorite-full');
  }
});