$('.trigger__name').on('click',function(e) {
  e.preventDefault();
  var btnAttr = $(this).attr('data-val'),
      input = $(this).siblings('.trigger__val');
  input.val(btnAttr);
  if ( $(this).attr('data-type') == "checked" ) {
    input.prop("checked", true);
  } else {
    input.prop("checked", false);
  }
});