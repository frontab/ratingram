$('.posts-excerpt__rating-btn').on('click', function(e) {
  e.preventDefault();

  var num = parseInt($(this).find('.posts-excerpt__rating-num')
                     .text());

  $(this).find('.posts-excerpt__rating-num').text(++num);

  $(this).addClass('active');

  $(this).closest('.posts-excerpt__rating')
         .addClass('selected');
});