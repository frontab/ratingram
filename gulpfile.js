global.site = {
  name: "ratingram"
}

global.$ = {
  tasks: require('./gulp/paths/tasks.js'),
  gulp: require('gulp'),
  del: require('del'),
  fs: require('fs'),
  bs: require('browser-sync').create(),
  gp: require('gulp-load-plugins')()
};

require('./gulp/paths/paths.js');

$.tasks.forEach(function(task) {
  require(task)();
});

$.gulp.task('default', $.gulp.series(
  'clean', 'json', 'iconfont',
  $.gulp.parallel(
    'pug', 'styles:dev', 'js:dev', 'img:dev', 'fonts'
  ),
  $.gulp.parallel(
    'watch',
    'server'
  )
));

$.gulp.task('view', $.gulp.series(
  $.gulp.parallel( 'styles:view', 'img:view'),
  'ftp'
));

$.gulp.task('complete', $.gulp.series(
  'clean', 'json', 'iconfont',
  $.gulp.parallel(
    'pug', 'styles:view', 'js:dev', 'img:view', 'fonts'
  )
));