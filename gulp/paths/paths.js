global.folder = {
  src: "./build"
};

global.ftp = {
  src: folder.src + "/**/*",
  host: "a266093.ftp.mchost.ru",
  user: "a266093_aelor",
  pass: "090892ppioip",
  folder: "/httpdocs/view/" + site.name
};

global.pug = {
  src: "./src/pages/*.pug",
  watch: ["./src/pages/*.pug",
          "./src/blocks/**/*.pug",
          "./src/templates/*.pug",
          "./src/json/*.json"],
  dest: folder.src + "/"
};

global.json = {
  name: "main.json",
  src: "./src/blocks/**/*.json",
  watch: "./src/blocks/**/*.json",
  dest: "./src/json/"
};

global.styles = {
  name: "main.scss",
  src: "./src/styles/main.scss",
  watch: ["./src/blocks/**/*.scss",
          "./src/styles/**/*.scss"],
  dest: folder.src + "/css"
};

global.js = {
  name: "main.js",
  src: {
    dev: ["./src/plugins/jquery/jquery.js",
          "./src/plugins/**/*.js",
          "./src/blocks/**/*.js"]
  },
  watch: ["./src/blocks/**/*.js",
          "./src/plugins/**/*.js"],
  dest: folder.src + "/js"
};

global.img = {
  src: ["./src/blocks/**/img/*.{jpg,png,gif,svg}",
        "./src/icons/favicons/*.{ico,png,svg,jpg}"],
  watch: ["./src/blocks/**/img/*.{jpg,png,gif,svg}",
        "./src/icons/favicons/*.{ico,png,svg,jpg}"],
  dest: folder.src + "/img"
};

global.fonts = {
  src: "./src/fonts/**/*.*",
  watch: "./src/fonts/**/*.*",
  dest: folder.src + "/fonts"
};

global.iconfont = {
  src: "./src/icons/svg/*.svg",
  watch: "./src/icons/svg/*.svg",
  dest: "./src/fonts/icons/",
  targetPath: "../../styles/utils/_icons.scss",
  fontPath: "../fonts/icons/",
  pathTamplate: "./src/styles/utils/_templates.scss",
  fontName: "icons"
};