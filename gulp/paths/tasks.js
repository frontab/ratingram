module.exports = [
  './gulp/tasks/clean',
  './gulp/tasks/server',
  './gulp/tasks/watch',
  './gulp/tasks/pug',
  './gulp/tasks/json',
  './gulp/tasks/styles',
  './gulp/tasks/scripts',
  './gulp/tasks/images',
  './gulp/tasks/fonts',
  './gulp/tasks/iconfont',
  './gulp/tasks/ftp'
];