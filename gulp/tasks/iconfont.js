module.exports = function() {

  var runTimestamp = Math.round(Date.now()/1000);

  $.gulp.task('iconfont', ()=> {
    return $.gulp.src(iconfont.src)
      .pipe($.gp.iconfontCss({
        path: iconfont.pathTemplate,
        fontName: iconfont.fontName,
        targetPath: iconfont.targetPath,
        fontPath: iconfont.fontPath
      }))
      .pipe($.gp.iconfont({
        fontName: iconfont.fontName,
        prependUnicode: true,
        formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
        timestamp: runTimestamp
      }))
      .pipe($.gulp.dest(iconfont.dest))
      .pipe($.bs.reload({
        stream: true
      }));
  });

};