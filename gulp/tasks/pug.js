module.exports = function() {

  $.gulp.task('pug', ()=> {
    return $.gulp.src(pug.src)
      .pipe($.gp.pug({
        locals: {
          main: JSON.parse($.fs.readFileSync(json.dest + json.name, 'utf-8')),
        },
        pretty: true
      }))
      .on('error', $.gp.notify.onError(function(error) {
        return {
          title: 'Pug',
          message: error.message
        };
      }))
      .pipe($.gulp.dest(pug.dest))
      .pipe($.bs.reload({
        stream: true
      }));
  });

};