module.exports = function() {

  $.gulp.task('ftp', ()=> {
    return $.gulp.src(ftp.src)
      .pipe($.gp.ftp({
        host: ftp.host,
        user: ftp.user,
        pass: ftp.pass,
        remotePath: ftp.folder
      }))
      .pipe($.gp.util.noop());
  });

};