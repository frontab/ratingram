module.exports = function() {

  $.gulp.task('styles:dev', ()=> {
    return $.gulp.src(styles.src)
      .pipe($.gp.sourcemaps.init())
      .pipe($.gp.sass())
      .on('error', $.gp.notify.onError(function(error) {
        return {
          title: 'Styles',
          message: error.message
        };
      }))
      .pipe($.gp.autoprefixer({
        browsers: ['last 8 version']
      }))
      .pipe($.gp.groupCssMediaQueries())
      .pipe($.gp.sourcemaps.write())
      .pipe($.gulp.dest(styles.dest))
      .pipe($.bs.reload({
        stream: true
      }));
  });

  $.gulp.task('styles:view', ()=> {
    return $.gulp.src(styles.src)
      .pipe($.gp.sass())
      .pipe($.gp.autoprefixer({
        browsers: ['last 8 version']
      }))
      .pipe($.gp.groupCssMediaQueries())
      .pipe($.gulp.dest(styles.dest));
  });

};