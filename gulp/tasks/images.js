module.exports = function() {

  $.gulp.task('img:dev', ()=> {
    return $.gulp.src(img.src)
      .pipe($.gp.rename({
        dirname: ''
      }))
      .pipe($.gulp.dest(img.dest))
      .pipe($.bs.reload({
        stream: true
      }));
  });

  $.gulp.task('img:view', ()=> {
    return $.gulp.src(img.src)
      .pipe($.gp.rename({
        dirname: ''
      }))
      .pipe($.gp.imagemin())
      .pipe($.gulp.dest(img.dest));
  });

};